import React from 'react';
import { withKnobs } from "@storybook/addon-knobs";
import { LoadingBar, ThemeProvider, CloverTheme, StarTheme, CherryTheme } from '../src';

export default {
  title: 'LoadingBar',
  decorators: [withKnobs],
};

export const cloverThemeLoadingBar = () =>
  <ThemeProvider theme={CloverTheme}>
    <LoadingBar />
    <LoadingBar />
    <LoadingBar />
  </ThemeProvider>

export const starThemeLoadingBar = () =>
  <ThemeProvider theme={StarTheme}>
    <LoadingBar />
    <LoadingBar />
    <LoadingBar />
  </ThemeProvider>

export const cherryThemeLoadingBar = () =>
  <ThemeProvider theme={CherryTheme}>
    <LoadingBar />
    <LoadingBar />
    <LoadingBar />
  </ThemeProvider>
