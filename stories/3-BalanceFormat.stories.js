import React from 'react';
import { withKnobs, text } from "@storybook/addon-knobs";
import { BalanceFormat, ThemeProvider, CloverTheme } from '../src';

export default {
  title: 'BalanceFormat',
  decorators: [withKnobs],
};

export const smallPositiveBalanceFormat = () =>
  <ThemeProvider theme={CloverTheme}>
    <BalanceFormat size='small'>
      {text('Text', '500')}
    </BalanceFormat>
  </ThemeProvider>

export const normalPositiveBalanceFormat = () =>
  <ThemeProvider theme={CloverTheme}>
    <BalanceFormat>
      {text('Text', '500')}
    </BalanceFormat>
  </ThemeProvider>

export const largePositiveBalanceFormat = () =>
  <ThemeProvider theme={CloverTheme}>
    <BalanceFormat size='large'>
      {text('Text', '500')}
    </BalanceFormat>
  </ThemeProvider>

export const smallNegativeBalanceFormat = () =>
  <ThemeProvider theme={CloverTheme}>
    <BalanceFormat size='small'>
      {text('Text', '-500')}
    </BalanceFormat>
  </ThemeProvider>

export const normalNegativeBalanceFormat = () =>
  <ThemeProvider theme={CloverTheme}>
    <BalanceFormat>
      {text('Text', '-500')}
    </BalanceFormat>
  </ThemeProvider>

export const largeNegativeBalanceFormat = () =>
  <ThemeProvider theme={CloverTheme}>
    <BalanceFormat size='large'>
      {text('Text', '-500')}
    </BalanceFormat>
  </ThemeProvider>
