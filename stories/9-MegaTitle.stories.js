import React from 'react';
import { withKnobs, text } from "@storybook/addon-knobs";
import { ThemeProvider, CloverTheme, MegaTitle } from '../src';

export default {
  title: 'MegaTitle',
  decorators: [withKnobs],
};

export const megaTitle = () =>
  <ThemeProvider theme={CloverTheme}>
    <MegaTitle>
      {text('Text', 'Mega Title')}
    </MegaTitle>
  </ThemeProvider>
