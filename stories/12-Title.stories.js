import React from 'react';
import { withKnobs, text } from "@storybook/addon-knobs";
import { Logo, Title, ThemeProvider, CloverTheme, StarTheme, CherryTheme } from '../src';

export default {
  title: 'Title',
  decorators: [withKnobs],
};

export const cloverThemeTitle = () =>
  <ThemeProvider theme={CloverTheme}>
    <Title>
      {text('Text', 'This is a title')}
    </Title>
  </ThemeProvider>

export const starThemeTitle = () =>
  <ThemeProvider theme={StarTheme}>
    <Title>
      {text('Text', 'This is a title')}
    </Title>
  </ThemeProvider>

export const CherryThemeTitle = () =>
  <ThemeProvider theme={CherryTheme}>
    <Title>
      {text('Text', 'This is a title')}
    </Title>
  </ThemeProvider>