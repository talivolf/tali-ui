import React from 'react';
import { action } from '@storybook/addon-actions';
import { withKnobs, text } from "@storybook/addon-knobs";
import { Button, ThemeProvider, CloverTheme, StarTheme, CherryTheme } from '../src';

export default {
  title: 'Button',
  decorators: [withKnobs],
};

export const cloverThemeNormalSizeButtonSquare = () =>
  <ThemeProvider theme={CloverTheme}>
    <Button
      size='normal'
      onClick={action('clicked')}>{text('Text', 'Hello')}</Button>
  </ThemeProvider>

export const cloverThemeNormalSizeButtonRound = () =>
  <ThemeProvider theme={CloverTheme}>
    <Button
      size='normal'
      round
      onClick={action('clicked')}>{text('Text', 'Hello')}</Button>
  </ThemeProvider>

export const cloverThemeSmallSizeButtonSquare = () =>
  <ThemeProvider theme={CloverTheme}>
    <Button
      size='small'
      onClick={action('clicked')}>{text('Text', 'Hello')}</Button>
  </ThemeProvider>

export const cloverThemeSmallSizeButtonRound = () =>
  <ThemeProvider theme={CloverTheme}>
    <Button
      size='small'
      round
      onClick={action('clicked')}>{text('Text', 'Hello')}</Button>
  </ThemeProvider>

export const cloverThemeMediumSizeButtonSquare = () =>
  <ThemeProvider theme={CloverTheme}>
    <Button
      size='medium'
      onClick={action('clicked')}>{text('Text', 'Hello')}</Button>
  </ThemeProvider>

export const cloverThemeMediumSizeButtonRound = () =>
  <ThemeProvider theme={CloverTheme}>
    <Button
      size='medium'
      round
      onClick={action('clicked')}>{text('Text', 'Hello')}</Button>
  </ThemeProvider>

export const cloverThemeLargeSizeButtonSquare = () =>
  <ThemeProvider theme={CloverTheme}>
    <Button
      size='large'
      onClick={action('clicked')}>{text('Text', 'Hello')}</Button>
  </ThemeProvider>

export const cloverThemeLargeSizeButtonRound = () =>
  <ThemeProvider theme={CloverTheme}>
    <Button
      size='large'
      round
      onClick={action('clicked')}>{text('Text', 'Hello')}</Button>
  </ThemeProvider>

// Star theme

export const starThemeNormalSizeButtonSquare = () =>
  <ThemeProvider theme={StarTheme}>
    <Button
      size='normal'
      onClick={action('clicked')}>{text('Text', 'Hello')}</Button>
  </ThemeProvider>

export const starThemeNormalSizeButtonRound = () =>
  <ThemeProvider theme={StarTheme}>
    <Button
      size='normal'
      round
      onClick={action('clicked')}>{text('Text', 'Hello')}</Button>
  </ThemeProvider>

export const starThemeSmallSizeButtonSquare = () =>
  <ThemeProvider theme={StarTheme}>
    <Button
      size='small'
      onClick={action('clicked')}>{text('Text', 'Hello')}</Button>
  </ThemeProvider>

export const starThemeSmallSizeButtonRound = () =>
  <ThemeProvider theme={StarTheme}>
    <Button
      size='small'
      round
      onClick={action('clicked')}>{text('Text', 'Hello')}</Button>
  </ThemeProvider>

export const starThemeMediumSizeButtonSquare = () =>
  <ThemeProvider theme={StarTheme}>
    <Button
      size='medium'
      onClick={action('clicked')}>{text('Text', 'Hello')}</Button>
  </ThemeProvider>

export const starThemeMediumSizeButtonRound = () =>
  <ThemeProvider theme={StarTheme}>
    <Button
      size='medium'
      round
      onClick={action('clicked')}>{text('Text', 'Hello')}</Button>
  </ThemeProvider>

export const starThemeLargeSizeButtonSquare = () =>
  <ThemeProvider theme={StarTheme}>
    <Button
      size='large'
      onClick={action('clicked')}>{text('Text', 'Hello')}</Button>
  </ThemeProvider>

export const starThemeLargeSizeButtonRound = () =>
  <ThemeProvider theme={StarTheme}>
    <Button
      size='large'
      round
      onClick={action('clicked')}>{text('Text', 'Hello')}</Button>
  </ThemeProvider>

// Cherry theme

export const cherryThemeNormalSizeButtonSquare = () =>
  <ThemeProvider theme={CherryTheme}>
    <Button
      size='normal'
      onClick={action('clicked')}>{text('Text', 'Hello')}</Button>
  </ThemeProvider>

export const cherryThemeNormalSizeButtonRound = () =>
  <ThemeProvider theme={CherryTheme}>
    <Button
      size='normal'
      round
      onClick={action('clicked')}>{text('Text', 'Hello')}</Button>
  </ThemeProvider>

export const cherryThemeSmallSizeButtonSquare = () =>
  <ThemeProvider theme={CherryTheme}>
    <Button
      size='small'
      onClick={action('clicked')}>{text('Text', 'Hello')}</Button>
  </ThemeProvider>

export const cherryThemeSmallSizeButtonRound = () =>
  <ThemeProvider theme={CherryTheme}>
    <Button
      size='small'
      round
      onClick={action('clicked')}>{text('Text', 'Hello')}</Button>
  </ThemeProvider>

export const cherryThemeMediumSizeButtonSquare = () =>
  <ThemeProvider theme={CherryTheme}>
    <Button
      size='medium'
      onClick={action('clicked')}>{text('Text', 'Hello')}</Button>
  </ThemeProvider>

export const cherryThemeMediumSizeButtonRound = () =>
  <ThemeProvider theme={CherryTheme}>
    <Button
      size='medium'
      round
      onClick={action('clicked')}>{text('Text', 'Hello')}</Button>
  </ThemeProvider>

export const cherryThemeLargeSizeButtonSquare = () =>
  <ThemeProvider theme={CherryTheme}>
    <Button
      size='large'
      onClick={action('clicked')}>{text('Text', 'Hello')}</Button>
  </ThemeProvider>

export const cherryThemeLargeSizeButtonRound = () =>
  <ThemeProvider theme={CherryTheme}>
    <Button
      size='large'
      round
      onClick={action('clicked')}>{text('Text', 'Hello')}</Button>
  </ThemeProvider>
