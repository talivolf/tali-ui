import React from 'react';
import { withKnobs, text } from "@storybook/addon-knobs";
import { NavBar, Logo, ThemeProvider, CloverTheme, StarTheme, CherryTheme } from '../src';

export default {
  title: 'NavBar',
  decorators: [withKnobs],
};

export const cloverThemeNavbar = () =>
  <ThemeProvider theme={CloverTheme}>
    <NavBar>
      <Logo>
        {text('Text', '🍀 YOUR WEBSITE')}
      </Logo>
    </NavBar>
  </ThemeProvider>

export const starThemeNavbar = () =>
  <ThemeProvider theme={StarTheme}>
    <NavBar>
      <Logo>
        {text('Text', '🌟 YOUR WEBSITE')}
      </Logo>
    </NavBar>
  </ThemeProvider>

export const cherryThemeNavbar = () =>
  <ThemeProvider theme={CherryTheme}>
    <NavBar>
      <Logo>
        {text('Text', '🍒 YOUR WEBSITE')}
      </Logo>
    </NavBar>
  </ThemeProvider>
