import React from 'react';
import { withKnobs, text } from "@storybook/addon-knobs";
import { SideBar, ThemeProvider, CloverTheme, StarTheme, CherryTheme } from '../src';

export default {
  title: 'SideBar',
  decorators: [withKnobs],
};

export const cloverThemeSideBar = () =>
  <ThemeProvider theme={CloverTheme}>
    <div style={{ height: 500 }}>
      <SideBar />
    </div>
  </ThemeProvider>

export const starThemeSideBar = () =>
  <ThemeProvider theme={StarTheme}>
    <div style={{ height: 500 }}>
      <SideBar />
    </div>
  </ThemeProvider>

export const cherryThemeSideBar = () =>
  <ThemeProvider theme={CherryTheme}>
    <div style={{ height: 500 }}>
      <SideBar />
    </div>
  </ThemeProvider>
