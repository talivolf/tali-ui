import React from 'react';
import { withKnobs, text } from "@storybook/addon-knobs";
import { RoundImage, ThemeProvider, CloverTheme, StarTheme, CherryTheme } from '../src';

export default {
  title: 'RoundImage',
  decorators: [withKnobs],
};

export const cloverThemeNormalRoundImage = () =>
  <ThemeProvider theme={CloverTheme}>
    <RoundImage type='primary'>
    </RoundImage>
  </ThemeProvider>

export const cloverThemeSmallRoundImage = () =>
  <ThemeProvider theme={CloverTheme}>
    <RoundImage type='primary' size='small'>
    </RoundImage>
  </ThemeProvider>

export const starThemeNormalRoundImage = () =>
  <ThemeProvider theme={StarTheme}>
    <RoundImage type='primary'>
    </RoundImage>
  </ThemeProvider>

export const starThemeSmallRoundImage = () =>
  <ThemeProvider theme={StarTheme}>
    <RoundImage type='primary' size='small'>
    </RoundImage>
  </ThemeProvider>

export const CherryThemeNormalRoundImage = () =>
  <ThemeProvider theme={CherryTheme}>
    <RoundImage type='primary'>
    </RoundImage>
  </ThemeProvider>

export const CherryThemeSmallRoundImage = () =>
  <ThemeProvider theme={CherryTheme}>
    <RoundImage type='primary' size='small'>
    </RoundImage>
  </ThemeProvider>