import React from 'react';
import { withKnobs, text } from "@storybook/addon-knobs";
import { Error, ThemeProvider, CloverTheme } from '../src';

export default {
  title: 'Error',
  decorators: [withKnobs],
};

export const error = () =>
  <ThemeProvider theme={CloverTheme}>
    <Error>
      {text('Text', 'This is an Error!')}
    </Error>
  </ThemeProvider>

