import React from 'react';
import { withKnobs } from "@storybook/addon-knobs";
import { Input, ThemeProvider, CloverTheme } from '../src';

export default {
  title: 'Input',
  decorators: [withKnobs],
};

export const input = () =>
  <ThemeProvider theme={CloverTheme}>
    <Input placeholder='placeolder' />
  </ThemeProvider >