import React from 'react';
import { withKnobs, text } from "@storybook/addon-knobs";
import { Label, ThemeProvider, CloverTheme } from '../src';

export default {
  title: 'Label',
  decorators: [withKnobs],
};

export const label = () =>
  <ThemeProvider theme={CloverTheme}>
    <Label>
      {text('Text', 'This is a label')}
    </Label>
  </ThemeProvider >
