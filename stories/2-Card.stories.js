import React from 'react';
import { number, withKnobs, text } from "@storybook/addon-knobs";
import { Card, ThemeProvider, CloverTheme, StarTheme, CherryTheme } from '../src';



export default {
  title: 'Card',
  decorators: [withKnobs],
};

const loremImpsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque ante sem, rhoncus ac nisi at mollis condimentum sem. Nam felis augue, accumsan eu lectus nec, gravida pretium quam. Nunc vehicula turpis non enim malesuada, in tempor ex varius. Pellentesque dignissim purus vitae pharetra rutrum. Suspendisse orci quam, tinciduntvitae tempus vel, efficitur elementum ex. Mauris euismod in odio a varius. Mauris quis nulla eget tortor tincidunt scelerisque. Etiam id ipsum eget dolor placerat fringilla. Integer erat velit, ultrices sit amet libero ac, semperfeugiat purus."

const knobOptions = {
  range: true,
  min: 100,
  max: 1000,
  step: 1,
};

export const cloverThemeSecondaryTypeCard = () =>
  <ThemeProvider theme={CloverTheme}>
    <Card
      type='secondary'
      px="10px"
      width={number('Width', 350, knobOptions)}
      height={number('Height', 350, knobOptions)}>

      {text('Text', loremImpsum)}
    </Card>
  </ThemeProvider>


export const cloverThemeInverseTypeCard = () =>
  <ThemeProvider theme={CloverTheme}>
    <Card
      type='inverse'
      px="10px"
      width={number('Width', 350, knobOptions)}
      height={number('Height', 350, knobOptions)}>
      {text('Text', loremImpsum)}
    </Card>
  </ThemeProvider>

export const starThemeSecondaryTypeCard = () =>
  <ThemeProvider theme={StarTheme}>
    <Card
      type='secondary'
      px="10px"
      width={number('Width', 350, knobOptions)}
      height={number('Height', 350, knobOptions)}>
      {text('Text', loremImpsum)}
    </Card>
  </ThemeProvider>


export const starThemeInverseTypeCard = () =>
  <ThemeProvider theme={StarTheme}>
    <Card
      type='inverse'
      px="10px"
      width={number('Width', 350, knobOptions)}
      height={number('Height', 350, knobOptions)}>
      {text('Text', loremImpsum)}
    </Card>
  </ThemeProvider>

export const cherryThemeSecondaryTypeCard = () =>
  <ThemeProvider theme={CherryTheme}>
    <Card
      type='secondary'
      px="10px"
      width={number('Width', 350, knobOptions)}
      height={number('Height', 350, knobOptions)}>
      {text('Text', loremImpsum)}
    </Card>
  </ThemeProvider>


export const cherryThemeInverseTypeCard = () =>
  <ThemeProvider theme={CherryTheme}>
    <Card
      type='inverse'
      px="10px"
      width={number('Width', 350, knobOptions)}
      height={number('Height', 350, knobOptions)}>
      {text('Text', loremImpsum)}
    </Card>
  </ThemeProvider>