import React from 'react';
import { withKnobs, text } from "@storybook/addon-knobs";
import { ThemeProvider, CloverTheme, StarTheme, CherryTheme, Logo, NavBar } from '../src';

export default {
  title: 'Logo',
  decorators: [withKnobs],
};

export const cloverThemeLogo = () =>
  <ThemeProvider theme={CloverTheme}>
    <NavBar>
      <Logo>
        {text('Text', '🍀 Logo')}
      </Logo>
    </NavBar>
  </ThemeProvider >

export const starThemeLogo = () =>
  <ThemeProvider theme={StarTheme}>
    <NavBar>
      <Logo>
        {text('Text', '🌟 Logo')}
      </Logo>
    </NavBar>

  </ThemeProvider>

export const cherryThemeLogo = () =>
  <ThemeProvider theme={CherryTheme}>
    <NavBar>
      <Logo>
        {text('Text', '🍒 Logo')}
      </Logo>
    </NavBar>

  </ThemeProvider>

