const fs = require('fs');
const execa = require('execa');

const versionType = process.argv[2] || 'patch';
if (['major', 'minor', 'patch'].includes(versionType) === false) {
  console.error(`Usage: node utils/publish.js [major/minor/patch]`);
  process.exit(1);
}

const pkg = JSON.parse(fs.readFileSync('package.json', 'utf-8'));
const [major, minor, patch] = pkg.version.split('.');
const version = { major, minor, patch };
version[versionType]++;
pkg.version = `${version.major}.${version.minor}.${version.patch}`;

fs.writeFileSync('package.json', JSON.stringify(pkg, null, 2));

execa('npm', ['publish'])
  .then(() => console.log(`version ${pkg.version} published successfully! 🎉🎉🎉`))
  .catch(() => console.error(`Could not publish version ${pkg.version}`))