# tali-ui

[![NPM](https://img.shields.io/npm/v/tali-ui.svg)](https://www.npmjs.com/package/tali-ui) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

![Themes](https://i.ibb.co/MgdqccJ/logo.png)

Tali-ui is **a simple UI library** that provides basic *React* components for your project.

## Storybook
To watch the components in action, [visit our Storybook](http://cranky-booth-831996.netlify.com/)

## Quick Install

### NPM

```bash
npm i tali-ui
```

### Yarn

```bash
yarn add tali-ui
```

## Import
After installation, you can import library components into your project like this:

```jsx
import { Component } from 'tali-ui';
```

# Usage

## Themes

We provide several basic themes:

![Themes](https://i.ibb.co/FgCszV7/theme-prev.jpg)

You can use our themes in the following way:
```jsx
import { ThemeProvider, CloverTheme } from 'tali-ui'

class ThemeExample extends Component {
  render () {
    return (
      <ThemeProvider theme={CloverTheme}>
        ...
      </ThemeProvider>
    )
  }
}
```



## Example 
```jsx
import React, { Component } from 'react'

import { ComponentExample , ThemeProvider, CloverTheme } from 'tali-ui'

class ComponentExample extends Component {
  render () {
    return (
      <ThemeProvider theme={CloverTheme}>
        <ComponentExample size="normal">Hello</ComponentExample>
      </ThemeProvider>
    )
  }
}
```

## List of components
* Button
* BalanceFormat
* Card
* Container
* Dialog
* Error
* Input
* Label
* LoadingBar
* LoginBox
* LoginForm
* Logo
* MegaTitle
* NavBar
* RoundImage
* SideBar
* Title
* TopBar