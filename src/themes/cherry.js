const darkRed = '#a80000';
const white = '#fff';
const red = '#fc8d8d';
const green = '#00db88';
const lightRed = '#fff2fb';
const darkWhite = '#f9f9f9';
const gray = '#d8dede';
const black = '#000';


export const theme = {
  palette: {
    body: darkWhite,
    text: lightRed,
    primary: darkRed,
    primaryText: lightRed,
    secondary: gray,
    secondaryText: black,
    inverse: lightRed,
    inverseText: black,
    positive: green,
    negative: red,
    dark: darkWhite,
  },
  dimensions: {
    roundImage: {
      normal: { width: '200px', height: '200px' },
      small: { width: '100px', height: '100px' }
    },

    button: {
      small: { padding: '5px', borderRadius: '3px', fontSize: 'inherit', minWidth: '100px' },
      normal: { padding: '20px', borderRadius: '10px', fontSize: '2em', minWidth: '280px' },
      medium: { padding: '20px', borderRadius: '10px', fontSize: '2em', minWidth: '400px' },
      large: { padding: '50px', borderRadius: '10px', fontSize: '6em', minWidth: '550px' },
    }
  },
  fonts: {
    main: `-apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen",
    "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue",
    sans-serif`,
    code: `source-code-pro, Menlo, Monaco, Consolas, "Courier New",
    monospace`
  }
};
