const darkGray = "#272121";
const darkGreen = "#185D22";
const lightRed = "#f6e9e9";
const green = "#00DB88";
const red = "#FC8D8D";

export const theme = {
  palette: {
    body: "#363333",
    text: lightRed,
    primary: darkGreen,
    primaryText: lightRed,
    secondary: darkGray,
    secondaryText: '#d8dede',
    inverse: lightRed,
    inverseText: "#363333",
    positive: green,
    negative: red,
    dark: "rgb(54, 51, 51)",
  },
  dimensions: {
    roundImage: {
      normal: { width: '200px', height: '200px' },
      small: { width: '100px', height: '100px' }
    },

    button: {
      small: { padding: '5px', borderRadius: '3px', fontSize: 'inherit', minWidth: '100px' },
      normal: { padding: '20px', borderRadius: '10px', fontSize: '2em', minWidth: '280px' },
      medium: { padding: '20px', borderRadius: '10px', fontSize: '2em', minWidth: '400px' },
      large: { padding: '50px', borderRadius: '10px', fontSize: '6em', minWidth: '550px' },
    }
  },
  fonts: {
    main: `-apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen",
    "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue",
    sans-serif`,
    code: `source-code-pro, Menlo, Monaco, Consolas, "Courier New",
    monospace`
  }
};
