import styled from "styled-components";


export const Title = styled.h1`
  font-size: 4em;
  text-shadow: 1px 1px 10px black;

  a {
    text-decoration: none;
    background-color: rgba(255, 255, 255, 0.4);
    border-radius: 5px;
    display: block;

    color: rgba(45, 177, 75, 1);
  }

  @media screen and (max-width: 1024px) {
    font-size: 3em;
  }

`;
