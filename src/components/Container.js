import styled  from "styled-components";
import { Flex } from "reflexbox/styled-components";


export const Container = styled(Flex).attrs({
  height: "100%",
  width: "100%",
  justifyContent: "center",
  alignItems: "center",
  flexDirection: "column"
})`
  background-image: url(${props => props.backgroundImage});
  background-repeat: no-repeat;
`;
