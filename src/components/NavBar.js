import styled from "styled-components";
import { Flex } from "reflexbox/styled-components";


export const NavBar = styled(Flex).attrs({
  as: "header",
  width: "100%",
  height: "50px",
  alignItems: "center",
  px: "10px"
})`
  background-color: ${props => props.theme.palette[props.type || 'primary']};

  h1 {
    margin: 0;
    color: ${props => props.theme.palette[(props.type || 'primary') + 'Text']}
  }
`;