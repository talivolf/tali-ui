import React from 'react';
import renderer from 'react-test-renderer';

import { Logo } from './Logo';
import { ThemeProvider, CloverTheme } from '../';

describe('Logo', () => {
  it('renders correctly', () => {
    const tree = renderer
      .create(
        <ThemeProvider theme={CloverTheme}>
          <Logo>Clover</Logo>
        </ThemeProvider>
      )
      .toJSON();

    expect(tree).toMatchSnapshot();
  });
});