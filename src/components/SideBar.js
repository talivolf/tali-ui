import styled  from "styled-components";
import { Flex } from "reflexbox/styled-components";


export const SideBar = styled(Flex).attrs({
  as: "aside",
  width: "200px",
  height: "100%",
  p: "10px",
  flexDirection: "column",
  alignItems: "center",
})`
  background-color: ${props => props.theme.palette.secondary};
  color: ${props => props.theme.palette.secondaryText};

  hr {
    background-color: currentColor;
    border: none;
    width: 100%;
    height: 1px;
  }
`;