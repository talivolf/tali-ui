import React from 'react';
import renderer from 'react-test-renderer';

import { LoginForm } from './LoginForm';
import { ThemeProvider, CloverTheme } from '../';

describe('LoginForm', () => {
  it('renders correctly', () => {
    const tree = renderer
      .create(
        <ThemeProvider theme={CloverTheme}>
          <LoginForm />
        </ThemeProvider>
      )
      .toJSON();

    expect(tree).toMatchSnapshot();
  });
});