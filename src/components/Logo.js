import styled from "styled-components";


export const Logo = styled.span`
  font-size: 2em;
  color: ${props => props.theme.palette.primaryText};
  text-decoration: none;
`;