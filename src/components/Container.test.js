import React from 'react';
import renderer from 'react-test-renderer';

import { Container } from './Container';
import { ThemeProvider, CloverTheme } from '../';

describe('Container', () => {
  it('renders correctly', () => {
    const tree = renderer
      .create(
        <ThemeProvider theme={CloverTheme}>
          <Container>Hello</Container>
        </ThemeProvider>
      )
      .toJSON();

    expect(tree).toMatchSnapshot();
  });
});