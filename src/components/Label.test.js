import React from 'react';
import renderer from 'react-test-renderer';

import { Label } from './Label';
import { ThemeProvider, CloverTheme } from '../';

describe('Label', () => {
  it('renders correctly', () => {
    const tree = renderer
      .create(
        <ThemeProvider theme={CloverTheme}>
          <Label>Credit Card</Label>
        </ThemeProvider>
      )
      .toJSON();

    expect(tree).toMatchSnapshot();
  });
});