import React from 'react';
import renderer from 'react-test-renderer';

import { Input } from './Input';
import { ThemeProvider, CloverTheme } from '../';

describe('Input', () => {
  it('renders correctly', () => {
    const tree = renderer
      .create(
        <ThemeProvider theme={CloverTheme}>
          <Input placeholder="Credit card number" />
        </ThemeProvider>
      )
      .toJSON();

    expect(tree).toMatchSnapshot();
  });
});