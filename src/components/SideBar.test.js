import React from 'react';
import renderer from 'react-test-renderer';

import { SideBar } from './SideBar';
import { ThemeProvider, CloverTheme } from '../';

describe('SideBar', () => {
  it('renders correctly', () => {
    const tree = renderer
      .create(
        <ThemeProvider theme={CloverTheme}>
          <SideBar></SideBar>
        </ThemeProvider>
      )
      .toJSON();

    expect(tree).toMatchSnapshot();
  });
});