import React from 'react';
import renderer from 'react-test-renderer';

import { Dialog } from './Dialog';
import { ThemeProvider, CloverTheme } from '../';

describe('Dialog', () => {
  it('renders correctly', () => {
    const tree = renderer
      .create(
        <ThemeProvider theme={CloverTheme}>
          <Dialog>Hello!</Dialog>
        </ThemeProvider>
      )
      .toJSON();

    expect(tree).toMatchSnapshot();
  });
});