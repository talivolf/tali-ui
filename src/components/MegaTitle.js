import styled from "styled-components";


export const MegaTitle = styled.h1`
  font-size: 8em;
  text-shadow: 1px 1px 20px black;
  margin-bottom: 0;
  
  @media screen and (max-width: 1024px) {
    font-size: 7em;
  }
`;