import React from 'react';
import renderer from 'react-test-renderer';

import { LoadingBar } from './LoadingBar';
import { ThemeProvider, CloverTheme } from '../';

describe('LoadingBar', () => {
  it('renders correctly', () => {
    const tree = renderer
      .create(
        <ThemeProvider theme={CloverTheme}>
          <LoadingBar type="primary" />
        </ThemeProvider>
      )
      .toJSON();

    expect(tree).toMatchSnapshot();
  });
});