import React from 'react';
import renderer from 'react-test-renderer';

import { TopBar } from './TopBar';
import { ThemeProvider, CloverTheme } from '../';

describe('TopBar', () => {
  it('renders correctly', () => {
    const tree = renderer
      .create(
        <ThemeProvider theme={CloverTheme}>
          <TopBar>Hello</TopBar>
        </ThemeProvider>
      )
      .toJSON();

    expect(tree).toMatchSnapshot();
  });
});