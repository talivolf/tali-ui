import styled from "styled-components";


export const Label = styled.label`
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  font-size: 1.5em;
  color: ${props => props.theme.palette.secondaryText};
`;
