import styled from "styled-components";


export const Dialog = styled.dialog`
  top: 0;
  bottom: 0;
  border: none;
  background-color: ${props => props.theme.palette.body};
  color: ${props => props.theme.palette.text};
  box-shadow: 4px 4px 5px 0px rgba(0, 0, 0, 0.59);
  z-index: 1;
`;