import React from 'react';
import renderer from 'react-test-renderer';

import { RoundImage } from './RoundImage';
import { ThemeProvider, CloverTheme } from '../';

describe('RoundImage', () => {
  it('renders correctly', () => {
    const tree = renderer
      .create(
        <ThemeProvider theme={CloverTheme}>
          <RoundImage type='primary' src="hello.png" />
        </ThemeProvider>
      )
      .toJSON();

    expect(tree).toMatchSnapshot();
  });
});