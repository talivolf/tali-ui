import styled from "styled-components";


export const LoginBox = styled.main.attrs({ width: "500px" })`
  background-color: ${props => props.theme.palette.secondary};
  box-shadow: 1px 1px 5px black;
`;
