import styled  from "styled-components";
import { Flex } from "reflexbox/styled-components";


export const Card = styled(Flex).attrs({
  alignItems: "center",
})`
  ${props => props.width && `width: ${props.width};`}
  ${props => props.height && `height: ${props.height};`}
  ${props => props.float && `float: ${props.float};`} 
  ${props => (props.py || props.px) && `padding: ${props.py || 0} ${props.px || 0};`}   
  ${props => (props.mt || props.ml) && `margin: ${props.mt || 0} ${props.ml || 0};`}   
  background-color: ${props => props.theme.palette[props.type || 'inverse']};
  color: ${props => props.theme.palette[(props.type || 'inverse') + 'Text']};
  box-shadow: 4px 4px 5px 0px rgba(0, 0, 0, 0.59);
`
