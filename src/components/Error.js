import styled from "styled-components";


export const Error = styled.p`
  text-align: center;
  color: ${props => props.theme.palette.negative};
`;