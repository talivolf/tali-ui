import styled, { keyframes } from "styled-components";


export const RoundImage = styled.img`
width: ${props => props.theme.dimensions.roundImage[props.size || 'normal'].width};
min-width: ${props => props.theme.dimensions.roundImage[props.size || 'normal'].width};
height: ${props => props.theme.dimensions.roundImage[props.size || 'normal'].height};
min-height: ${props => props.theme.dimensions.roundImage[props.size || 'normal'].height}
border-radius: 100%;
object-fit: cover;
border: 5px solid ${props => props.theme.palette[props.type]};
mix-blend-mode: "multiply";
background-color: white;
object-position: ${props => props.position || 'initial'};
`;
