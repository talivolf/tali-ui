import styled from "styled-components";


export const Button = styled.button`
  cursor: pointer;
  font-weight: bold;
  background-color: ${props => props.theme.palette.primary};
  color: ${props => props.theme.palette.primaryText};
  border: none;
  text-decoration: none;
  ${props => (props.py || props.px) && `padding: ${props.py || 0} ${props.px || 0};`}   
  ${props => (props.mt || props.ml) && `margin: ${props.mt || 0} ${props.ml || 0};`} 

  border-radius: ${props => props.round ? '100%' : props.theme.dimensions.button[props.size || 'normal'].borderRadius};
  padding: ${props => props.theme.dimensions.button[props.size || 'normal'].padding};
  font-size: ${props => props.theme.dimensions.button[props.size || 'normal'].fontSize};
  min-width: ${props => props.theme.dimensions.button[props.size || 'normal'].minWidth}
  ${props => props.size === 'small' && 'height: fit-content;'}
  
  ${props => props.size === 'large' && `
    @media screen and (max-width: 1400px) {
      transform: scale(0.7);
    }`}

    position: relative;

    ${props => props.round && `
      align-self: center;
      font-size: 2.5em;
      font-weight: bold;
      box-shadow: 1px 1px 10px var(${props.theme.palette.secondary});
      height: ${props.theme.dimensions.button[props.size || 'normal'].minWidth}

      &:focus {
        outline: none;
      }

      &:active {
        box-shadow: none;
      }

      &:before {
        content: "";
        position: absolute;
        border-radius: 100%;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        background: linear-gradient(
          150deg,
          rgba(255, 255, 255, 0.2),
          rgba(255, 255, 255, 0.05)
        );
      }

      &:active:before {
        display: none;
      }
    `}
`;