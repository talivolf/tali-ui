import styled  from "styled-components";


export const BalanceFormat = styled.h2`
  margin-top: 0;
  margin-bottom: 0.5em;
  color: ${props => parseInt(props.children) > 0 ? props.theme.palette.positive : props.theme.palette.negative};
  ${props => props.size === 'large' && 'font-size: 6em;'}
  ${props => props.size === 'small' && 'font-weight: normal; font-size: 16px; margin-bottom: 0;'}
`;