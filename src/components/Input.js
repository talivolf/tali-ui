import styled from "styled-components";


export const Input = styled.input.attrs({ type: "text" })`
  display: inline-block;
  width: 100%;
  box-sizing: border-box;
  border: none;
  border-radius: 3px; 
  font-size: 20px;
  margin: 10px 0;
  padding: 5px;
`;