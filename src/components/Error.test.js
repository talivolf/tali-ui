import React from 'react';
import renderer from 'react-test-renderer';

import { Error } from './Error';
import { ThemeProvider, CloverTheme } from '../';

describe('Error', () => {
  it('renders correctly', () => {
    const tree = renderer
      .create(
        <ThemeProvider theme={CloverTheme}>
          <Error>All your base are belong to us</Error>
        </ThemeProvider>
      )
      .toJSON();

    expect(tree).toMatchSnapshot();
  });
});