import React from 'react';
import renderer from 'react-test-renderer';

import { Card } from './Card';
import { ThemeProvider, CloverTheme } from '../';

describe('Card', () => {
  it('renders correctly', () => {
    const tree = renderer
      .create(
        <ThemeProvider theme={CloverTheme}>
          <Card type="inverse">Hello!</Card>
        </ThemeProvider>
      )
      .toJSON();

    expect(tree).toMatchSnapshot();
  });
});