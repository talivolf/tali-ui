import React from 'react';
import renderer from 'react-test-renderer';

import { LoginBox } from './LoginBox';
import { ThemeProvider, CloverTheme } from '../';

describe('LoginBox', () => {
  it('renders correctly', () => {
    const tree = renderer
      .create(
        <ThemeProvider theme={CloverTheme}>
          <LoginBox />
        </ThemeProvider>
      )
      .toJSON();

    expect(tree).toMatchSnapshot();
  });
});