import React from 'react';
import renderer from 'react-test-renderer';

import { NavBar } from './NavBar';
import { ThemeProvider, CloverTheme } from '../';

describe('NavBar', () => {
  it('renders correctly', () => {
    const tree = renderer
      .create(
        <ThemeProvider theme={CloverTheme}>
          <NavBar>
            Hello
          </NavBar>
        </ThemeProvider>
      )
      .toJSON();

    expect(tree).toMatchSnapshot();
  });
});