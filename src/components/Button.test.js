import React from 'react';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';

import { Button } from './Button';
import { ThemeProvider, CloverTheme } from '../';

describe('Button', () => {
  it('renders correctly', () => {
    const tree = renderer
      .create(
        <ThemeProvider theme={CloverTheme}>
          <Button size="small">Small</Button>
          <Button size="normal">Normal</Button>
          <Button size="medium">Medium</Button>
          <Button size="large">Large</Button>
          <Button size="normal" round>Round</Button>
        </ThemeProvider>
      )
      .toJSON();

    expect(tree).toMatchSnapshot();
  });

  it('clicks', () => {
    const mockClick = jest.fn();
    const button = shallow((<Button size="normal" onClick={mockClick}>Hello</Button>));
    button.simulate('click');
    expect(mockClick.mock.calls.length).toEqual(1);
  })
});