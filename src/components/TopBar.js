import styled  from "styled-components";
import { Flex } from "reflexbox/styled-components";


export const TopBar = styled(Flex).attrs({
  as: "header",
  width: "100%",
  height: "50px",
  justifyContent: "space-between",
  p: "10px"
})`
  background-color: ${props => props.theme.palette[props.type || 'primary']};
  box-shadow: 1px 1px 5px 0px rgba(0, 0, 0, 0.59);
`;
