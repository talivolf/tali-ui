import React from 'react';
import renderer from 'react-test-renderer';

import { Title } from './Title';
import { ThemeProvider, CloverTheme } from '../';

describe('Title', () => {
  it('renders correctly', () => {
    const tree = renderer
      .create(
        <ThemeProvider theme={CloverTheme}>
          <Title>Hello</Title>
        </ThemeProvider>
      )
      .toJSON();

    expect(tree).toMatchSnapshot();
  });
});