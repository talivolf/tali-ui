import React from 'react';
import renderer from 'react-test-renderer';

import { MegaTitle } from './MegaTitle';
import { ThemeProvider, CloverTheme } from '../';

describe('MegaTitle', () => {
  it('renders correctly', () => {
    const tree = renderer
      .create(
        <ThemeProvider theme={CloverTheme}>
          <MegaTitle />
        </ThemeProvider>
      )
      .toJSON();

    expect(tree).toMatchSnapshot();
  });
});