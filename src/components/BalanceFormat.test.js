import React from 'react';
import renderer from 'react-test-renderer';

import { BalanceFormat } from './BalanceFormat';
import { ThemeProvider, CloverTheme } from '../';

describe('BalanceFormat', () => {
  it('renders correctly', () => {
    const tree = renderer
      .create(
        <ThemeProvider theme={CloverTheme}>
          <BalanceFormat size="large">-100</BalanceFormat>
          <BalanceFormat size="large">100</BalanceFormat>
        </ThemeProvider>
      )
      .toJSON();

    expect(tree).toMatchSnapshot();
  });
});